require "application_system_test_case"

class FichasTest < ApplicationSystemTestCase
  setup do
    @ficha = fichas(:one)
  end

  test "visiting the index" do
    visit fichas_url
    assert_selector "h1", text: "Fichas"
  end

  test "creating a Ficha" do
    visit fichas_url
    click_on "New Ficha"

    fill_in "Descripcion", with: @ficha.descripcion
    fill_in "Fecha", with: @ficha.fecha
    fill_in "Titulo", with: @ficha.titulo
    click_on "Create Ficha"

    assert_text "Ficha was successfully created"
    click_on "Back"
  end

  test "updating a Ficha" do
    visit fichas_url
    click_on "Edit", match: :first

    fill_in "Descripcion", with: @ficha.descripcion
    fill_in "Fecha", with: @ficha.fecha
    fill_in "Titulo", with: @ficha.titulo
    click_on "Update Ficha"

    assert_text "Ficha was successfully updated"
    click_on "Back"
  end

  test "destroying a Ficha" do
    visit fichas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ficha was successfully destroyed"
  end
end
