json.extract! ficha, :id, :titulo, :fecha, :descripcion, :created_at, :updated_at
json.url ficha_url(ficha, format: :json)
