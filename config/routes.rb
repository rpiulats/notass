Rails.application.routes.draw do
  
  root "fichas#index"
  resources :fichas
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
