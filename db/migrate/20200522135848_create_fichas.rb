class CreateFichas < ActiveRecord::Migration[5.2]
  def change
    create_table :fichas do |t|
      t.string :titulo
      t.date :fecha
      t.text :descripcion

      t.timestamps
    end
  end
end
